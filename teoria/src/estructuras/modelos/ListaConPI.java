/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructuras.modelos;

/**
 *
 * @author DJCotelo
 */
public interface ListaConPI<E> {
    void inicio();
    void siguiente();
    void eliminar();
    void fin();
    void insertar(E e);
    E recuperar();
    boolean esFin();
    boolean esVacia();
    int talla();    
}
