/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructuras.modelos;

/**
 *
 * @author Cotel
 */
public interface Pila<E> {
    void apilar(E e);
    
    /** Si !esVacia() **/
    E cima();
    E desapilar();
    
    boolean esVacia();
}
