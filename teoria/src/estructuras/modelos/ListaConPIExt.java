/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructuras.modelos;

/**
 *
 * @author DJCotelo
 */
public interface ListaConPIExt<E> extends ListaConPI<E>{
    void buscar(E e);
    void vaciar();
    int talla();
    void invertir();
    void eliminar(E e);
    boolean eliminarUltimo(E e);
}
