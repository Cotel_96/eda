/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructuras.lineales;

/**
 *
 * @author DJCotelo
 */
public class Nodo<E> {
    
    public E elemento;
    public Nodo siguiente;
    
    public Nodo(E e) {
        this.elemento = e;
        this.siguiente = null;
    }
    
    public Nodo(E e, Nodo siguiente) {
        this.elemento = e;
        this.siguiente = siguiente;
    }

    public E getElemento() {
        return elemento;
    }

    public void setElemento(E elemento) {
        this.elemento = elemento;
    }

    public Nodo getSiguiente() {
        return siguiente;
    }

    public void setSiguiente(Nodo siguiente) {
        this.siguiente = siguiente;
    }
    
    
    
}
