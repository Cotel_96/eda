package estructuras.lineales;

import estructuras.modelos.Cola;
import java.util.NoSuchElementException;

public class ArrayCola<E> implements Cola<E> {
    protected E elArray[];
    protected static final int CAPACIDAD = 30000;
    protected int finalC, principioC, talla;
    
    public ArrayCola() {
        elArray = (E[]) new Object[CAPACIDAD];
        talla = 0; principioC = 0; finalC = -1;
    }
    
    public String toString() {
        String res = "";
        int aux = principioC;
        for(int i = 0; i<talla; i++, aux=incrementar(aux)){
            res += elArray[aux].toString()+"|"; 
        }
        return res;
    }
    
    private void duplicarArrayCola() {
        E[] nuevo = (E[]) new Object[elArray.length*2];
        finalC = principioC;
        principioC = 0;
        for(int i=0; i<elArray.length;){
            nuevo[i++] = elArray[finalC];
            finalC = incrementar(finalC);
        }
        finalC = talla-1;
        elArray = nuevo;
        
    }
    
    private int incrementar(int indice) {
        if( ++indice==elArray.length) indice=0;
        return indice;
    }
    
    public E primero() {
        if(talla == 0) throw new NoSuchElementException("Cola vacia");
        else return elArray[principioC];
    }
    
    public void encolar(E e) {
        if(talla==elArray.length) duplicarArrayCola();
        elArray[finalC] = e;
        finalC = incrementar(finalC);
        talla++;
    }
    
    public E desencolar() {
        if(talla == 0) {
            throw new NoSuchElementException("Cola vacia");
        } else {
            E aux = elArray[principioC];
            principioC = incrementar(principioC);
            talla--;
            return aux;
        }
    }
    
    public boolean esVacia() {
        return talla==0;
    }    
}
