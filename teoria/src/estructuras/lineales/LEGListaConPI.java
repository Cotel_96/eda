/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructuras.lineales;

import estructuras.modelos.ListaConPI;
import estructuras.lineales.Nodo;
import java.util.NoSuchElementException;

/**
 *
 * @author DJCotelo
 */
public class LEGListaConPI<E> implements ListaConPI<E>{
    protected Nodo<E> primero, PI, antPI;
    protected int talla;
    
    public LEGListaConPI() {
        primero = null;
        PI = null;
        antPI = null;
        talla = 0;
    }

    @Override
    public void inicio() {
       PI = primero;
       antPI = null;
    }

    @Override
    public void siguiente() {
        if (PI == null) throw new NoSuchElementException("Final de la lista");
        antPI = PI;
        PI = PI.siguiente;
    }

    @Override
    public void eliminar() {
        E aux = (E) PI.elemento;
        if (PI == null) throw new NoSuchElementException("Final de la lista");
        else if (primero == PI) {
            primero = primero.siguiente;
        } else {
            antPI.siguiente = PI.siguiente;
        }
        PI = PI.siguiente;
        talla--;
    }

    @Override
    public void fin() {
        while(!esFin()) {
            this.siguiente();
        }
    }

    @Override
    public void insertar(E e) {
        if(primero == PI) {
            primero = new Nodo<E>(e, primero);
            antPI = primero;
        } else {
            antPI.siguiente = new Nodo(e, PI);
            antPI = antPI.siguiente;
        }
        talla++;
    }

    @Override
    public E recuperar() {
        return PI.elemento;
    }

    @Override
    public boolean esFin() {
        return PI == null;
    }

    @Override
    public boolean esVacia() {
        return talla == 0;
    }

    @Override
    public int talla() {
        return talla;
    }
}
