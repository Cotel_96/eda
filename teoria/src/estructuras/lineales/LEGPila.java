/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructuras.lineales;

import estructuras.modelos.Pila;
import java.util.NoSuchElementException;

/**
 *
 * @author DJCotelo
 */
public class LEGPila<E> implements Pila<E> {
    
    protected Nodo<E> cima;
    protected int numElem;
    
    public LEGPila() {
        cima = null;
        numElem = 0;
    }

    @Override
    public void apilar(E e) {
        cima = new Nodo(e, cima);
        numElem++;
    }

    @Override
    public E cima() {
        if (numElem == 0) throw new NoSuchElementException("Pila vacia");
        else return cima.elemento;
    }

    @Override
    public E desapilar() {
            if (numElem == 0) throw new NoSuchElementException("Pila vacia");
            else {
                E dato = cima.elemento;
                numElem--;
                cima = cima.siguiente;
                return dato;
            }
    }

    @Override
    public boolean esVacia() {
        return numElem == 0;
    }
    
}
