/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructuras.lineales;

import java.util.NoSuchElementException;

/**
 *
 * @author DJCotelo
 */
public class LEGPilaExt<E extends Comparable<E>> extends LEGPila<E> {
    
    public LEGPilaExt() {
        super();
    }
    
    public E eliminaMenor() {
        Nodo<E> aux = this.cima;
        E menor = this.cima.elemento;
        while(!this.esVacia()) {
            if(menor == null || aux.elemento.compareTo(menor) < 0) menor = aux.elemento;
            aux = aux.siguiente;
        }
        return menor;
    }
    
    public E eliminaMenorMetodos() {
        if (esVacia()) throw new NoSuchElementException("Pila vacia");
        E dato = desapilar();
        E minResto = eliminaMenorMetodos();
        apilar(dato);
        if(minResto == null || dato.compareTo(minResto) < 0) return dato;
        return minResto;
    }
    
}
