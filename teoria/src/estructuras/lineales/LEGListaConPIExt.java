/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructuras.lineales;

import estructuras.modelos.ListaConPIExt;

/**
 *
 * @author DJCotelo
 */
public class LEGListaConPIExt<E> extends LEGListaConPI<E> implements ListaConPIExt<E> {

    @Override
    public int talla() {
        int n = 0;
        for (inicio(); !esFin(); siguiente()) n++;
        return n;
    }

    @Override
    public void vaciar() {
        inicio();
        while(!esVacia()) {
            eliminar();
        }
    }

    @Override
    public void invertir() {
        if(!esVacia()) {
            inicio();
            E dato = recuperar();
            eliminar();
            invertir();
            insertar(dato);
        }
    }

    @Override
    public void eliminar(E e) {
        inicio();
        while(!esFin()) {
            if(recuperar().equals(e)) eliminar();
            else siguiente();
        }
    }

    @Override
    public void buscar(E e) {
        inicio();
        for(int i=0; i<talla() && !(recuperar().equals(e)); i++) {
            siguiente();
        }
    }

    @Override
    public boolean eliminarUltimo(E e) {
        boolean res = false;
        inicio();
        invertir();
        buscar(e);
        if(!esFin()) {
            eliminar();
            res = true;
        }
        invertir();
        return res;
    }

}
