package estructuras.lineales;

import estructuras.modelos.Pila;
import java.util.NoSuchElementException;

public class ArrayPila<E> implements Pila<E> {
    protected E[] elArray;
    protected int talla;
    private final int CAPACIDAD = 30000;
    
    public ArrayPila() {
        elArray = (E[]) new Object[CAPACIDAD];
        talla = 0;
    }
    
    public boolean esVacia() {
        return talla == 0;
    }
    
    public E cima() {
        if(talla == 0) throw new NoSuchElementException("Pila vacia");
        else return elArray[talla -1];
    }
    
    public void apilar(E e) {
        if(talla == elArray.length) {
            duplicarArrayPila();
        }
        elArray[talla] = e;
        talla++;
    }
    
    public E desapilar() {
        if(talla == 0) throw new NoSuchElementException("Pila vacia");
        else {
            E dato = elArray[--talla];
            return dato;
        }
    }
    
    private void duplicarArrayPila() {
        E[] aux = elArray;
        elArray = (E[]) new Object[aux.length * 2];
        for(int i=0; i<talla; i++) {
            elArray[i] = aux[i];
        }
    }    
}
