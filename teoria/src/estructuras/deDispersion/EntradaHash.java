public class EntradaHash<C,V> {
	public C clave;
	public V valor;

	public EntradaHash(C clave, V valor) {
		this.clave = clave;
		this.valor = valor;
	}

	public C getClave() { return this.clave; }
	public V getValor() { return this.valor; }
	public void setClave(C c) { this.clave = c; }
	public void setValor(V v) { this.valor = v; }
}