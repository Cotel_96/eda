package estructuras.deDispersion;

import estructuras.modelos.Mapa;
import estructuras.modelos.ListaConPI;
import estructuras.lineales.LEGListaConPI;

public class TablaHash<C,V> implements Mapa<C,V> {
	
	private ListaConPI<EntradaHash<C,V>> elArray[];
	private int talla;

	public TablaHash(int tallaMaximaEstimada) {
		int capacidad = siguientePrimo((int) (tallaMaximaEstimada/0.75));
		elArray = new LEGListaConPI[capacidad];
		for (int i=0; i<elArray.length; i++) {
			elArray[i] = new LEGListaConPI<EntradaHash<C,V>>();
		}
		talla = 0;
	}

	protected int indiceHash(C c) {
		int indiceHash = c.hashCode() % this.elArray.length;
		if(indiceHash < 0) {
			indiceHash += this.elArray.length;
		}
		return indiceHash;
	}

	protected static final int siguientePrimo(int n) {
		if(n % 2 == 0) n++;
		for( ; !esPrimo(n); n+=2);
		return n;
	}

	protected static final boolean esPrimo(int n) {
		for(int i=3; i*i <= n; i+=2)
			if(n%i == 0) return false;
		return true;
	}

	public V insertar(C c, V v) {
		V antiguoValor = null;
		int pos = indiceHash(c);
		ListaConPI<EntradaHash<C,V>> cubeta = elArray[pos];
		for(cubeta.inicio(); !cubeta.esFin() && !cubeta.recuperar().clave.equals(c); cubeta.siguiente());
		if(cubeta.esFin()) {
			cubeta.insertar(new EntradaHash<C,V>(c,v));
			talla++;
		} else {
			antiguoValor = cubeta.recuperar().valor();
			cubeta.recuperar().valor = v;
		}
		return antiguoValor;
	}

	public V eliminar(C c) {
		int pos = indiceHash(c);
		ListaConPI<EntradaHash<C,V>> cubeta = elArray[pos];
		V valor = null; 
		for (cubeta.inicio(); !cubeta.esFin() && !cubeta.recuperar().clave.equals(c); cubeta.siguiente());
		if (!cubeta.esFin()) { 
		 	valor = cubeta.recuperar().valor;
		 	cubeta.eliminar();
		 	talla--;
		}
		return valor;
	}

	public V recuperar(C c) {
		int pos = indiceHash(c);
		ListaConPI<EntradaHash<C,V>> cubeta = elArray[pos];
		for(cubeta.inicio(); !cubeta.esFin() && !cubeta.recuperar().clave.equals(c); cubeta.siguiente());
		if(cubeta.esFin()) return null;
		else return cubeta.recuperar().valor();
	}

	public boolean esVacio() {
		return talla == 0;
	}

	public int talla() {
		return talla;
	}
}