/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritmos.aplicaciones;

import java.util.Arrays;


/**
 *
 * @author DJCotelo
 */
public class ProblemasTema2 {
    
    /**
     * mergeSort
     * Método que ordena un array dado. Divide la secuencia de n elementos a ordenar
     * en dos subsecuencias de n/2 elementos cada una. Ordena las dos subsecuencias
     * recursivamente utilizando "mergeSort". La recursión finaliza cuando la secuencia
     * a ordenar contiene un único elemento, en cuyo caso ya está ordenado.
     * Combina las dos subsecuencias utilizando "merge"
     * @param <T> = Tipo de elemento a ordenar
     * @param v = Array de elementos T
     * @param izq = Indice de inicio de la subsecuencia
     * @param der  = Indice del final de la subsecuencia
     */
    
    public static <T extends Comparable<T>> void mergeSort(T v[], int izq, int der) {
        if(izq < der) {
            int mitad = (izq+der)/2;
            mergeSort(v, izq, mitad);
            mergeSort(v, mitad+1, der);
            merge(v, izq, mitad+1 ,der);
        }
    }
    
    /**
     * merge
     * Método que fusiona dos subsecuencias ordenadas. Método auxiliar para "mergeSort"
     * @param <T> = Tipo de elemento a ordenar
     * @param v = Array de elementos T
     * @param izqA = Indice de inicio
     * @param izqB = Indice de la mitad
     * @param derB  = Indice del final
     */
    
    public static <T extends Comparable<T>> void merge(T v[], int izqA, int izqB, int derB) {
        int i = izqA;
        int derA = izqB -1 ;
        int j  = izqB;
        int k = 0;
        int r;
        
        T[] aux = (T[]) new Comparable[derB-izqA+1];
        while(i <= derA && j <= derB) {
            if(v[i].compareTo(v[j]) < 0)
                aux[k] = v[i++];
            else
                aux[k] = v[j++];
            k++;
        }
        for(r = i; r <= derA; r++) aux[k++] = v[r];
        for(r = j; r <= derB; r++) aux[k++] = v[r];
        for(k = 0, r = izqA; r <= derB; r++, k++) v[r] = aux[k];
    }
    
    /**
     * quickSort
     * Método que ordena un array dado. El array se particiona en dos subsecuencias
     * A[p..q] y A[q+1..r] de forma que los elementos de A[p..q] son menores o iguales
     * que los de A[q+1..r]. El índice q se calcula también en el procedimiento de la partición.
     * Los dos subvectores se ordenan recursivamente utilizando "quicksort". Como los 
     * subvectores se ordenan en su lugar correspondiente, no hay que hacer nada para combinar
     * las soluciones. El vector A[p..r] ya está ordenado.
     * @param <T> = Tipo de elemento a ordenar
     * @param v = Array de elementos T
     * @param izq = Índice de inicio del array
     * @param der  = Índice de final del array
     */
    
    public static <T extends Comparable<T>> void quickSort(T v[], int izq, int der) {
        if(izq < der) {
            int indiceP = particion(v, izq, der);
            quickSort(v, izq, indiceP -1);
            quickSort(v, indiceP +1, der);
        }
    }
    
    /**
     * particion
     * Método que equilibra las particiones de "quicksort". Método auxiliar de quicksort.
     * Reorganiza el vector A[p..r] dejando en A[p..q] los elementos menores o iguales que
     * en A[q+1..r]. Se toma un elemento pivote y se mueven los elementos menores
     * que el pivote de forma que queden los menores a la izquierda y los mayores
     * a la derecha.
     * @param <T> = Tipo de elemento a ordenar
     * @param v = Array de elementos T
     * @param izq = Índice de inicio
     * @param der = Índice de final
     * @return int
     */
    
    private static <T extends Comparable<T>> int particion(T v[], int izq, int der) {
        T pivote = mediana3(v, izq, der);
        int i = izq;
        int j = der-1;
        while(i<j) {
            while(pivote.compareTo(v[++i]) > 0);
            while(pivote.compareTo(v[--j]) < 0);
            intercambiar(v, i, j);
        }
        intercambiar(v, i, j);
        intercambiar(v, i, der-1);
        return i;
    }
    
    /**
     * intercambiar
     * Método que cambia la posición de dos elementos en un array. Método auxiliar
     * de "particion". Intercambia la posición del elemento A[ind1] por A[ind2] y viceversa.
     * @param <T> = Tipo de elemento a intercambiar
     * @param v = Array de elementos T
     * @param ind1 = Índice primer elemento
     * @param ind2  = Índice segundo elemento
     */
    
    private static <T> void intercambiar(T v[], int ind1, int ind2) {
        T tmp = v[ind1];
        v[ind1] = v[ind2];
        v[ind2] = tmp;
    }
    
    /**
     * mediana3
     * Método que devuelve la mediana de 3 elementos, devuelve el pivote. Método
     * auxiliar "particion"
     * @param <T> = Tipo de elemento del array
     * @param v = Array de elementos T
     * @param izq = Índice de inicio
     * @param der = Índice de final
     * @return Elemento T del array que se usará como pivote
     */
    
    private static <T extends Comparable<T>> T mediana3(T v[], int izq, int der) {
        int mid = (izq+der)/2;
        if(v[mid].compareTo(v[izq]) < 0) intercambiar(v, izq, mid);
        if(v[der].compareTo(v[izq]) < 0) intercambiar(v, izq, der);
        if(v[der].compareTo(v[mid]) < 0) intercambiar(v, mid, der);
        intercambiar(v, mid, der-1);
        return v[der-1];
    }
    
    /**
     * seleccion
     * Método que devuelve el menor elemento k-ésimo. El vector A[p..r] se particiona
     * en dos subvectores, de forma que los elementos del primero son menores o iguales
     * que el pivote y los del segundo son mayores o iguales. Buscamos el subvector
     * correspondiente haciendo llamadas recursivas al algoritmo. Si (k ≤ q) entonces el 
     * k-ésimo menor estará en A[p..q]. Si no, estará en A[q+1..r]
     * @param <T> = Tipo de elemento a buscar
     * @param v = Array de elementos T donde se va a buscar
     * @param p = Índice de inicio
     * @param r = Índice de final
     * @param k = Posición del elemento a buscar
     * @return Elemento T buscado
     */
    
    public static <T extends Comparable<T>> T seleccion(T v[], int p, int r, int k) {
        if(p == r) {
            return v[k];
        } else {
            int q = particion(v, p, r);
            if(k <= q) {
                return seleccion(v, p, q, k);
            } else {
                return seleccion(v, q+1, r, k);
            }
        }
    }
    
    public static double elevadoAN(double x, double n) {
        if(n == 0) return 1;
        if(n == 1) return x;
        double aux = Math.pow(x, n/2);
        if (n%2 == 0) {
            return aux*aux;
        } else {
            return aux*aux*x;
        }
    }    
   
    
    public static void main(String[] args) {
        Integer[] arr = {41, 633, 23, 1, 245, 77, 524, 23, 12, 2, 0, 675};
        quickSort(arr, 0, arr.length-1);
        System.out.println(Arrays.toString(arr));
    }
    
}
