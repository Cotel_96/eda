package librerias.estructurasDeDatos.grafos;


/**
 * Write a description of class VerticeCoste here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class VerticeCoste implements Comparable<VerticeCoste> {
    private int vertice;
    private double coste;
    
    public VerticeCoste(int v, double c) {
        this.vertice = v;
        this.coste = c;       
    }
    
    public int getVertice() {
        return this.vertice;
    }
    
    public double getCoste() {
        return this.coste;
    }
    
    public int compareTo(VerticeCoste aux) {
        if(this.coste > aux.getCoste()) return 1;
        else if(this.coste < aux.getCoste()) return -1;
        else return 0;
    }
}
